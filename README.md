Sample application with Spring Boot 2 and Waffle for MS domain authentication.

Configured for Negotiate authentication (NTLM disabled) and fall back to Waffle Basic authentication (that is, not based in Spring Basic filter, but in Waffle Basic filter).

Boot and browse http://localhost:8080/

By default, it allows ROLE_USER users (which is added by default by Waffle). 
